# blog

blogging with hugo and github pages + actions

## instructions

1. fork this repo
2. update the `baseURL` in [config.toml](config.yaml#L1)
3. change the github pages source branch to `gh-pages`

## developing

1. clone this repo
2. run `hugo serve` to build and serve the site
