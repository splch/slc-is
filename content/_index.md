---
title: Welcome to my site
---

I'm a senior software engineer and musician. My dreams are to create art with computers and help quantum grow big. I'm currently working for [IonQ](https://ionq.com/) to integrate quantum frameworks with their quantum computers.

The [Posts](/posts) page contains all my posts, [Music](/music) links to some recordings and AI generations, and [About](/about) serves as a repository for miscellaneous information _(contact, interests, etc.)_. My [study guide](https://slc.is/posts/study.html) is available as well.

Have a look around and feel free to share! 🥳
