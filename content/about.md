---
title: Who I am
---

```shell
~$ whoami
Spencer Churchill
```

**Feel free to contact me at spence@duck.com**

- Download my [PGP Key](https://keys.mailvelope.com/pks/lookup?op=get&search=spence@duck.com)
- Check out my [résumé](/data/resume/resume.pdf)

---

I'm always listening to or making music. But when I'm up for a challenge, I like to code all sorts of things. I make [chrome extensions](https://chrome.google.com/webstore/search/splch?_category=extensions) when I need tools and work on quantum and <abbr title="Artificial Intelligence">AI</abbr> projects when I want to push myself.

My current research interest is why different methods of <abbr title="True Random Number Generation">TRNG</abbr> approach correlation coefficients of 0 at different rates. A future goal of mine is to study how quantum will change optimizations like gradient decent. I'm actively involved in [IonQ](https://ionq.com/) but still need to study different methods of minimum finding.

---

Languages I'm Using:

- Python
- C
- Go
- <abbr title="HyperText Markup Language">HTML</abbr> / <abbr title="Cascading Style Sheets">CSS</abbr> / <abbr title="JavaScript">JS</abbr>

Languages I'm Learning:

- [Prolog](https://swish.swi-prolog.org/)
- [6502 Assembly](http://www.6502asm.com/)
