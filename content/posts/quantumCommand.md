---
title: Quantum Command Rulebook
description: Paper for the QBRS algorithm
date: 2023-06-06
categories:
  - Quantum
draft: false
---

```
   ____        _         ____              _       
  / /\ \      / \       / /\ \            / \      
 / /  \ \    / _ \     / /  \ \          / _ \     
/ /    > >  / /_\ \   / /    > >        / /_\ \    
\/     / / / _____ \  \/     / /       / _____ \   
       \/ /_/     \_\/       \/       /_/     \_\  
```

## **Objective**

Defeat your opponent by eliminating all of their Quantum Units using your knowledge of quantum mechanics!

## **Setup**

1. Start with 8 Quantum Units each on a 10x10 board.
2. Place your Quantum Units within the first three rows of your side. The front line is off-limits—no early game rushes allowed!
3. Each Quantum Unit begins with 100 quantum energy points.
4. Decide who goes first—a coin toss or any other fair method.

## **Turn Structure**

Every turn has three phases: **Movement**, **Action**, and **Recovery**. You must proceed in this order and cannot backtrack to a previous phase in the same turn.

### **1. Movement Phase**

- *Particle State:* Move your Quantum Units up to two squares in any direction, including diagonally. Movement is blocked by predefined obstacles.
- *Wave State:* Quantum Units can move up to three squares in any direction, even through obstacles!

### **2. Action Phase**

- *Particle State:* Attack adjacent enemy Quantum Units in all eight directions, causing them to lose quantum energy depending on a six-sided die roll.
- *Wave State:* Perform a Quantum Leap within a five-square range at the cost of quantum energy equivalent to ten times the distance leapt. The final landing square can be one square off in any direction due to quantum uncertainty.

### **3. Recovery Phase**

- A Quantum Unit can regain up to 50 quantum energy points by skipping its next turn, but it cannot exceed the starting 100 energy points.

## **Special Quantum Mechanics**

### **Quantum Entanglement**

If two Quantum Units interact (attack or move adjacent), they become entangled. If one changes state, the other will follow suit at the start of its next turn. The most recent entanglement dictates the state change.

### **Heisenberg Uncertainty**

After a Quantum Leap, the leaping unit disappears and reappears at the start of the next turn. It's untouchable during this period, avoiding both attacks and entanglements. This leap action costs quantum energy equal to ten times the leap's distance, with no additional costs.

### **Schrodinger's Box**

Once per game, place a Quantum Unit into superposition. It is simultaneously eliminated and active until an enemy unit moves adjacent, forcing a measurement with a 50/50 survival chance. This superposition effect lasts indefinitely until triggered, and this ability can be used at any phase and does not cost a turn.

## **Victory**

A Quantum Unit is considered "eliminated" when it loses all of its quantum energy. The game ends when all Quantum Units of one player are eliminated. If both players lose their last unit in the same turn, it's a quantum stalemate—a draw!

Remember, Quantum Command isn't just a game—it's a battle of wits, strategy, and a touch of unpredictability. Harness the power of quantum mechanics to outwit and outplay your opponent. Ready to command the quantum realm? Let's get started!

---

## **Frequently Asked Questions**

**Q1: Can a Quantum Unit move and attack in the same turn?**

A1: Yes. A Quantum Unit can move during the Movement Phase and then perform an attack in the Action Phase. Remember that attacking only applies to Quantum Units in Particle State. If the unit is in Wave State, it can perform a Quantum Leap instead of an attack.

**Q2: How does Quantum Entanglement work?**

A2: Quantum Entanglement is triggered when two Quantum Units interact, either through attacking or moving adjacent to one another. If one of the entangled Quantum Units changes its state (from Particle to Wave or vice versa), the other will also change its state at the start of its next turn. The state change is determined by the most recent entanglement.

**Q3: Can multiple Quantum Units become entangled at the same time?**

A3: Yes, multiple Quantum Units can become entangled. If a Quantum Unit interacts with several enemy units in the same turn, all of them will become entangled with that unit.

**Q4: How does Schrodinger's Box work?**

A4: Schrodinger's Box is a unique ability that can be used once per game for any Quantum Unit. When you activate Schrodinger's Box, your Quantum Unit enters a state of superposition. In this state, the Quantum Unit is considered both eliminated and active simultaneously until an enemy unit moves adjacent to it. At that point, a measurement is forced, and there's a 50% chance that your unit will survive and a 50% chance it will be eliminated. 

**Q5: Can a Quantum Unit move or attack while it's in superposition due to Schrodinger's Box?**

A5: No, a Quantum Unit in superposition cannot move or attack. It essentially exists in a limbo state until its fate is decided by an enemy unit moving adjacent to it.

**Q6: How does Heisenberg Uncertainty apply to the game?**

A6: Heisenberg Uncertainty applies to Quantum Leaps. After a Quantum Leap, the leaping unit disappears and becomes untouchable until the start of the next turn. This implies that the unit can avoid both attacks and entanglements during this period.

**Q7: What happens in a draw or a "quantum stalemate"?**

A7: If both players lose their last Quantum Unit in the same turn, the game ends in a draw, also called a "quantum stalemate". In this case, there is no winner, and the game may be replayed if the players wish.
