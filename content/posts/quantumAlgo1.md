---
title: Quantum Deep Dives #1 | Quantum Teleportation
description: A deep dive into the quantum teleportation algorithm
slug: qc1
date: 2023-02-02
categories:
  - Quantum
tags:
  - Deep Dive
draft: false
---

In this tutorial series we will hold a microscope to quantum algorithms to look as close as we can.

Let's consider the famous "spooky action at a distance."

## Quantum Teleportation

This quote refers to a concept known as Quantum Teleportation. This algorithm is a way to send information from one place to another; it's like having a magic box that can instantly transfer information about whatever is inside to somewhere else, without anything in between!

We can imagine we're at a circus and see a magician tapping their magic boxes with a wand. They have three boxes with an identical coin in each.

The magician begins the show by reaching into the first box and spinning the coin. Then they lock all the boxes up. Nobody can see inside the boxes anymore but they can hear the first coin spinning. The magician then performs a series of taps: they tap second box and the second coin begins to spin, then the second and third boxes together and the third coin begins spinning too, and then they tap the first and second boxes together. Finally they tap the first box one last time.

Now the magician opens the first and second boxes, and the coins stop spinning, showing the coins' faces. Knowing only how the two coins were facing, the magician taps twice and opens the third box and the last coin stops spinning exactly how the first coin stopped. The magician repeats this show every night and the first and third coins **always** land the same way.

Here, the coin is a quantum object and the wand is how they affect its state. Let's dive deeper and reveal this magician's secret.

## Quantum Circuit

To recreate the actions of the magician, we use a circuit like the one below.

```
q0 ───────────■───H───M───────■───
              │               │
q1 ───H───■───X───────M───■───│───
          │               │   │
q2 ───────X───────────────X───Z───M
```

The circuit is read from left to right. On the far left we have the three coins, or quantum bits (qubits `q0`, `q1`, `q2`), followed by wires that each apply a series of operations, or gates.

These gates are the following:

- `H`: Hadamard - puts the coin (qubit) in a spinning (superposition) state

- `X-■`: Controlled Not - flips the second coin (target qubit) if the first coin (control qubit) is in state heads (|1⟩)

- `Z-■`: Controlled Phase Flip - lets the coin (target qubit) spin half a rotation (applies a phase shift of -1) if the first coin (control qubit) is in state heads (|1⟩)

- `M`: Measurement - reads the state of the coin (qubit) and collapses it to either heads or tailes (|1⟩ or |0⟩)

The quantum teleportation circuit involves three qubits: the sender's qubit (`q0`), the qubit to be teleported (`q1`), and the receiver's qubit (`q2`).

**The circuit works as follows**:

The sender applies a Hadamard gate to their qubit (`q0`), which puts it into a superposition of the |0⟩ and |1⟩ states.

The sender and the qubit to be teleported (`q1`) are then entangled using a Controlled Not (`X-■`) gate. This gate flips the second qubit (`q1`) if the first qubit (`q0`) is in the |1⟩ state. As a result, the two qubits are now entangled and share a correlated state.

The sender then applies a Controlled Phase Flip (`Z-■`) gate to the two entangled qubits. This gate lets the qubit to be teleported (`q1`) spin half a rotation (applies a phase shift of -1) if the sender's qubit (`q0`) is in state |1⟩. This gate effectively transfers the information about the state of the sender's qubit (`q0`) to the qubit to be teleported (`q1`).

The sender then measures both their own qubit (`q0`) and the entangled qubit (`q1`). The measurement collapses the two qubits to either the |0⟩ or |1⟩ state, depending on the result of the measurement.

The measurements are then sent to the receiver via classical communication channels, such as phone or internet.

Finally, the receiver uses the measurement results to apply a series of gates to their own qubit (`q2`) in order to reconstruct the original state of the qubit to be teleported (`q1`). Specifically, if `q1` is |1⟩, `X` is applied, and if `q0` is |1⟩, `Z` is applied to `q2`. These two classical bits reconstruct a quantum state.

## Quantum Bit

In classical computing, a bit is the fundamental unit of information, either 0 or 1. In quantum computing, the fundamental unit of information is a qubit (short for "quantum bit"), which is a two-level quantum system that can exist in a superposition of both |0⟩ and |1⟩ at the same time.

Qubits can be implemented in various physical systems, such as the spin of an electron or the polarization of a photon. In the context of quantum teleportation, qubits are often implemented in the polarization of photons.

Polarization is a property of light that describes the orientation of its electric field. Polarization can be either vertical or horizontal, represented as |0⟩ and |1⟩, respectively. A photon can exist in a superposition of both polarizations at the same time, which is what makes it a useful qubit.

To perform operations on qubits, we use quantum gates, which are the equivalent of classical logic gates. In the context of quantum teleportation, we use gates such as the Hadamard gate, Controlled Not gate, and Controlled Phase Flip gate, which act on the polarization of the photons to create superposition and entanglement.

Superposition is the property of a qubit being in both states simultaneously, while entanglement is the property of two or more qubits being in a correlated state, even when separated by large distances. These properties are what make quantum teleportation possible.

When two qubits are entangled, measuring one of them will affect the state of the other, regardless of how far apart they are. This is the phenomenon of "spooky action at a distance" that Einstein referred to.

In the context of quantum teleportation, entangling the sender's qubit with the qubit to be teleported allows for the transfer of information about the state of the sender's qubit to the qubit to be teleported, even though the qubits themselves may be physically separated by large distances. This is possible because the qubits are entangled, and therefore their states are correlated.

The measurement of the sender's qubit and the entangled qubit collapses their states to either |0⟩ or |1⟩. This measurement is what transfers the information about the state of the sender's qubit to the qubit to be teleported. The receiver can then use this information to reconstruct the original state of the qubit to be teleported using a series of gates on their own qubit.

## Gate Pulses

Gate pulses are the physical operations applied to qubits in order to manipulate their state, create superpositions and entanglement, and perform quantum computations. These pulses are essentially electromagnetic fields that are applied to the qubits for a precise amount of time, typically on the order of nanoseconds or microseconds, depending on the physical system.

There are a variety of methods for implementing gate pulses in different physical systems. In superconducting qubits, for example, the gate pulses are typically microwave pulses that are applied to the qubit through a control line. These pulses have a frequency that is resonant with the energy level spacing of the qubit, and the pulse duration is calibrated to achieve the desired operation.

In ion trap qubits, gate pulses are typically laser pulses that are applied to the ion. The laser frequency is tuned to the energy level spacing of the ion, and the pulse duration is calibrated to achieve the desired operation.

Finally, photonic qubits use optical pulses that are manipulated using mirrors, beam splitters, wave plates, and other optical elements. These pulses are typically generated using lasers, and the pulse duration and polarization are carefully controlled to achieve the desired operation.

The calibration of gate pulses is a crucial step in the implementation of quantum algorithms, as even small errors in the pulse duration or frequency can lead to significant errors in the computation. Calibration is typically achieved through a process called pulse shaping, where the amplitude and duration of the pulse are adjusted to achieve the desired operation. In addition, feedback and error correction techniques can be used to further improve the accuracy of the gate pulses.

Once the gate pulses are applied to the qubits, their state can be measured using a variety of techniques. For example, in superconducting qubits, the state can be read out using a resonant circuit that is coupled to the qubit. When the qubit is in a particular state, it induces a current in the resonant circuit, which can be detected using a sensitive amplifier.

In ion trap qubits, the state can be measured using fluorescence. When the ion is illuminated with a laser pulse, it emits photons with a probability that depends on its state. By measuring the number of emitted photons, the state of the ion can be inferred.

Finally, photonic qubits can be measured using photon detectors that measure the polarization or phase of the incoming photon.

## Quantum Mechanics

Quantum mechanics is the fundamental theory that underlies the behavior of quantum systems, including qubits. In the context of quantum teleportation, quantum mechanics plays a key role in understanding how information is transferred from the sender's qubit to the qubit to be teleported.

Quantum mechanics tells us that a qubit exists in a superposition of states, meaning that it can be in both the |0⟩ and |1⟩ states simultaneously. The state of a qubit is represented by a mathematical object called a wavefunction, which is a complex vector in a two-dimensional Hilbert space. The wavefunction contains information about the probability of measuring the qubit in each of its possible states.

In the context of quantum teleportation, the sender's qubit and the qubit to be teleported are entangled. Entanglement is a property of two or more qubits being in a correlated state, even when separated by large distances. When two qubits are entangled, the state of one qubit is not determined until a measurement is made on the other qubit. This phenomenon is known as quantum nonlocality and is the basis for the "spooky action at a distance" that Einstein referred to.

In the quantum teleportation circuit, the sender applies a series of gates to their qubit, entangling it with the qubit to be teleported. The sender then measures both their own qubit and the entangled qubit. The measurement collapses the two qubits to either the |0⟩ or |1⟩ state, depending on the result of the measurement.

The measurement of the sender's qubit and the entangled qubit is what transfers the information about the state of the sender's qubit to the qubit to be teleported. This transfer of information is possible because of the entanglement between the two qubits. When the measurement is made, the wavefunction of the sender's qubit collapses to either the |0⟩ or |1⟩ state, and the wavefunction of the qubit to be teleported also collapses to a correlated state that depends on the measurement outcome. This correlated state contains information about the state of the sender's qubit.

The receiver can then use the measurement outcomes to apply a series of gates to their own qubit in order to reconstruct the original state of the qubit to be teleported. This reconstruction is possible because the measurement outcomes contain information about the state of the sender's qubit, which was transferred via the entangled qubit.

## Conclusion

We have now taken a deep dive into the quantum teleportation algorithm and the underlying principles of quantum mechanics that make it possible. We have explored the concept of qubits and their properties, including superposition and entanglement. We have also looked at the gate pulses used to manipulate the state of qubits and the measurement techniques used to read out their state.

We have seen how quantum mechanics plays a crucial role in understanding the behavior of qubits and how they can be used to transfer information through entanglement. The quantum teleportation algorithm demonstrates the power of quantum computing to solve problems that are difficult or impossible for classical computers to solve.

As we continue to explore quantum algorithms and their applications, we can expect to see further breakthroughs in fields such as cryptography, chemistry, and machine learning. The potential for quantum computing to revolutionize these fields and many others is enormous, and the deep dive into quantum teleportation is just the beginning of our journey into the fascinating world of quantum computing.
